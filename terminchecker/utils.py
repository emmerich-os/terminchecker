from typing import Optional

import terminchecker.html
import terminchecker.sms


def check_url_for_free_appointment_and_get_date(
    url: str, tor_password: str = None
) -> Optional[str]:
    """Check a list of URLs for a free appointment.

    Parameters:
        url : list
        tor_password : str
            Local TOR password.

    Returns:
        str or None
            Time of free appointment if one is available in an URL, `None` otherwise.

    """
    content = terminchecker.html.load_html_content_to_list(url, tor_password=tor_password)
    free_appointment, time_ = terminchecker.html.check_list_for_free_appointment(content)
    if free_appointment:
        date = terminchecker.html.get_date_from_url(url)
        datetime = terminchecker.html.add_time_to_datetime(date, time_)
        return terminchecker.html.format_datetime_to_str(datetime)
    return None
