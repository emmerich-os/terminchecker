FROM python:3.7
COPY ../ /app
WORKDIR /app
RUN pip install .
CMD ["terminchecker", "-w 4", "main:app"]
