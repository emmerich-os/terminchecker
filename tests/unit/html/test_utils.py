import datetime
from contextlib import nullcontext as doesnotraise
from unittest import mock

import pytest
import requests

import terminchecker.html


@pytest.mark.parametrize(
    "status_code,expected",
    [
        (200, "example_data_list"),
        (404, []),
    ],
)
def test_load_html_content_to_list(request, example_data_bytes, status_code, expected):
    response = requests.models.Response()
    response._content = example_data_bytes
    response.status_code = status_code

    with pytest.raises(type(expected)) if isinstance(expected, Exception) else doesnotraise():
        with mock.patch("torrequest.TorRequest") as mock_tr:
            mock_tr.return_value.__enter__().get.return_value = response
            result = terminchecker.html.load_html_content_to_list(
                "test_url", tor_password="test_pw"
            )
        if isinstance(expected, str):
            assert result == request.getfixturevalue(expected)
        else:
            assert result == expected


@pytest.mark.parametrize(
    "example_data,expected",
    [
        ([], (False, None)),
        ("example_data_list", (False, None)),
        ("example_data_list_with_free_appointment", (True, "12:00")),
    ],
)
def test_check_list_for_free_appointment(request, example_data, expected):
    content = (
        request.getfixturevalue(example_data) if isinstance(example_data, str) else example_data
    )
    result = terminchecker.html.check_list_for_free_appointment(content)

    assert result == expected


@pytest.mark.parametrize(
    "url,expected",
    [
        (
            (
                "https://netappoint.de/onlineticket/index.php?"
                "option=showbooking&company=lkgiessenkfz&cur_cause=0&auth="
                "&month=12&year=2020&sel=17#anker"
            ),
            datetime.datetime(2020, 12, 17),
        ),
        (
            (
                "https://netappoint.de/onlineticket/index.php?"
                "option=showbooking&company=lkgiessenkfz&cur_cause=0&auth="
                "&month=12&year=2020&sel=18#anker"
            ),
            datetime.datetime(2020, 12, 18),
        ),
        (
            (
                "https://netappoint.de/onlineticket/index.php?"
                "option=showbooking&company=lkgiessenkfz&cur_cause=0&auth="
                "&month=11&year=2020&sel=17#anker"
            ),
            datetime.datetime(2020, 11, 17),
        ),
        (
            (
                "https://netappoint.de/onlineticket/index.php?"
                "option=showbooking&company=lkgiessenkfz&cur_cause=0&auth="
                "&month=12&year=2021&sel=17#anker"
            ),
            datetime.datetime(2021, 12, 17),
        ),
        (
            (
                "https://netappoint.de/onlineticket/index.php?"
                "option=showbooking&company=lkgiessenkfz&cur_cause=0&auth="
                "&month=6&year=2021&sel=6#anker"
            ),
            datetime.datetime(2021, 6, 6),
        ),
    ],
)
def test_get_date_from_url(url, expected):
    result = terminchecker.html.get_date_from_url(url)

    assert result == expected


@pytest.mark.parametrize(
    "date,time,expected",
    [
        (datetime.datetime(2020, 1, 1), "12:00", datetime.datetime(2020, 1, 1, 12, 0)),
        (datetime.datetime(2020, 1, 1), "09:00", datetime.datetime(2020, 1, 1, 9, 0)),
        (datetime.datetime(2020, 1, 1), "9:15", datetime.datetime(2020, 1, 1, 9, 15)),
        (datetime.datetime(2020, 1, 1), "16:06", datetime.datetime(2020, 1, 1, 16, 6)),
    ],
)
def test_add_time_to_datetime(date, time, expected):
    result = terminchecker.html.add_time_to_datetime(date=date, time=time)
    assert result == expected


@pytest.mark.parametrize(
    "date,strformat,expected",
    [
        (datetime.datetime(2020, 1, 1, 12, 0), None, "01.01.2020 12:00"),
    ],
)
def test_format_datetime_to_str(date, strformat, expected):
    if strformat is None:
        result = terminchecker.html.format_datetime_to_str(date)
    else:
        result = terminchecker.html.format_datetime_to_str(date, strformat=strformat)

    assert result == expected
