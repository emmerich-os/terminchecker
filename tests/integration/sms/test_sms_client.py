import pathlib

import pytest

import terminchecker.sms

__my_loc__ = pathlib.Path(__file__).parent


@pytest.fixture()
def config_path():
    return __my_loc__ / "../../../vars.conf.template"


def test_from_configuration_file(config_path):
    result = terminchecker.sms.SMSClient.from_configuration_file(config_path)

    assert isinstance(result, terminchecker.sms.SMSClient)
    assert result.number == "some_phone_number"
    assert result.account_sid == "twilio_account_sid"
    assert result.auth_token == "some_auth_token"
