import configparser
import datetime
import pathlib
import time

import click

import terminchecker.html
import terminchecker.sms
from .utils import check_url_for_free_appointment_and_get_date


__my_loc__ = pathlib.Path(__file__).parent

URLS = [
    (
        "https://netappoint.de/onlineticket/index.php?option="
        "showbooking&company=lkgiessenkfz&cur_cause=0&auth=&month=12&year=2020&sel=18"
    ),
    (
        "https://netappoint.de/onlineticket/index.php?option="
        "showbooking&company=lkgiessenkfz&cur_cause=0&auth=&month=12&year=2020&sel=21"
    ),
    (
        "https://netappoint.de/onlineticket/index.php?option="
        "showbooking&company=lkgiessenkfz&cur_cause=0&auth=&month=12&year=2020&sel=22"
    ),
    (
        "https://netappoint.de/onlineticket/index.php?option="
        "showbooking&company=lkgiessenkfz&cur_cause=0&auth=&month=12&year=2020&sel=23"
    ),
]


@click.command()
@click.option(
    "path",
    "--config",
    "-c",
    type=click.Path(exists=True),
    default=__my_loc__ / "../vars.conf",
    help="path INI-like config",
    show_default=True,
)
@click.option(
    "urls",
    "--urls",
    "-u",
    type=list,
    default=URLS,
    help="list of URLs, separated by comma",
    show_default=False,
)
@click.option(
    "number",
    "--number",
    "-n",
    type=str,
    help="phone number to send the alarm SMS to",
)
def run_termin_checker(path, urls, number):
    run_termin_checker_func(path, urls, number)


def run_termin_checker_func(path, urls, number):
    config = configparser.ConfigParser()
    config.read(path)
    tor_password = config["tor"]["tor_password"]
    while True:
        sms_client = terminchecker.sms.SMSClient.from_configuration_file(path)
        print(f"Checking for free appointments ({datetime.datetime.now()})")
        for url in urls:
            time.sleep(1)
            free_date = check_url_for_free_appointment_and_get_date(url, tor_password=tor_password)
            if free_date is None:
                continue
            print(f"***FREE APPOINTMENT FOUND AT {free_date}***")
            print("Sending SMS")
            sms_client.send_sms(number, message=f"Freier Termin verfügbar: {free_date}")
        del sms_client
