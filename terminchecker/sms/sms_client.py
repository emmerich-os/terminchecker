import configparser
import pathlib

import twilio.rest


class SMSClient:
    def __init__(self, number: str, account_sid: str, auth_token: str):
        """Instantiate SMS client.

        Parameters:
            number : str
                Twilio phone number.
            account_sid : str
                Twilio account SID.
            auth_token : str
                Twilio authentication token.

        """
        self.number = number
        self.account_sid = account_sid
        self.auth_token = auth_token
        self._client = None

    @classmethod
    def from_configuration_file(cls, path: pathlib.Path):
        """Instantiate SMS client from INI-like config file."""
        config = configparser.ConfigParser()
        config.read(path)
        return cls(**config["twilio"])

    @property
    def client(self):
        if self._client is None:
            self._client = twilio.rest.Client(self.account_sid, self.auth_token)
        return self._client

    def send_sms(self, to: str, message: str = "Hello from TerminChecker!"):
        """Send SMS message to number.

        Parameters:
            to : str
                Number of recipient.
            message : str, optional
                Message content.
                Defaults to `"Hello from TerminChecker!"

        """
        self.client.messages.create(
            to=to,
            from_=self.number,
            body=message,
        )
