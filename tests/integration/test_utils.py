from unittest import mock

import pytest
import requests

import terminchecker.utils


@pytest.mark.parametrize(
    "url,website_content,expected",
    [
        (
            "https://test_url.de/&month=12&year=2020&sel=17#anker",
            "example_data_bytes",
            None,
        ),
        (
            "https://test_url.de/&month=12&year=2020&sel=17#anker",
            "example_data_bytes_with_free_appointment",
            "17.12.2020 12:00",
        ),
    ],
)
def test_check_url_for_free_appointment_and_get_date(request, url, website_content, expected):
    response = requests.models.Response()
    response._content = request.getfixturevalue(website_content)
    response.status_code = 200

    with mock.patch("torrequest.TorRequest") as mock_tr:
        mock_tr.return_value.__enter__().get.return_value = response
        result = terminchecker.utils.check_url_for_free_appointment_and_get_date(url)

    assert result == expected
