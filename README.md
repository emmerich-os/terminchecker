# TerminChecker

Checks, in a list of URLs, whether there is an available appointment.
This app is entirely focues on the KFZ-Meldestelle Gießen,
i.e. the https://netappoint.de/onlineticket/ website.

If there is an available appointment, the app sends an SMS to the
specified number.

## Usage
```bash
terminchecker -n '+491234567890'
```

The SMS service uses [twilio](https://www.twilio.com/). The credentials for the
API have to be set in a config file whose path has to be passed to the script as follows:

```bash
terminchecker -c /home/user/vars.conf -n '+491234567890'
```

A template for the config is located in the top-level of this repository (`vars.conf.temmplate`).


The URLs can be passed using the `-u/--urls` flag:

```bash
terminchecker -u https://url.de,https://url.de,...
```


This app uses the `torrequest` library.

### torrequest setup
Install tor
```bash
sudo apt-get update
sudo apt-get install tor
```

Restart the TOR service

```bash
sudo /etc/init.d/tor restart
```

Create a password

```bash
tor --hash-password <enter your password here>
```

The output is your hashed password. This will be needed in the next step.

Edit the `torrc` (usually located in `/etc/tor/torrc`.
Uncomment the Controlport and insert
```bash
SOCKSPort 9050
HashedControlPassword <your hashed passsword obtained earlier here>
CookieAuthentication 1

### This section is just for location-hidden services ###
```

Save and exit and restart TOR.

```bash
sudo /etc/init.d/tor restart
```

For detailed instructions see [here](https://www.scrapehero.com/make-anonymous-requests-using-tor-python/)
