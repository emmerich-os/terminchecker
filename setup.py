import setuptools


def read(fname, split=True):
    with open(fname) as f:
        content = f.read()
    return content.split("\n") if split else content


if __name__ == "__main__":
    print(__doc__)

    setuptools.setup(
        name="terminchecker",
        version="1.0",
        description="Termin checker able to send alarm SMS messages.",
        author="Fabian Emmerich",
        author_email="fabian@emmerich-mail.de",
        packages=setuptools.find_packages(include=("terminchecker*",)),
        python_requires=">=3.8",
        install_requires=read("requirements.txt"),
        include_package_data=True,
        entry_points="""
            [console_scripts]
            terminchecker=terminchecker.run:run_termin_checker
        """,
    )
