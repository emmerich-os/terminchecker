import datetime
import re
from typing import Optional
from typing import Tuple
from typing import Union

import requests
import torrequest


def load_html_content_to_list(url: str, tor_password) -> list:
    """Load HTML content of URL to list.

    Parameters:
        url : str
        tor_password : str
            Local TOR password.

    Returns:
        list

    """
    try:
        with torrequest.TorRequest(password=tor_password) as tr:
            response = tr.get(url)
            response.raise_for_status()
            content = response.text
            return content.split("\n")
    except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as e:
        print(e)
    return []


def check_list_for_free_appointment(content: list) -> Tuple[bool, Optional[str]]:
    """Check a list for available appointments.

    Parameters:
        content : list

    Returns:
        bool
            `True` if free appointment found, `False` otherwise.
        str or None
            Time of the free appointment if available, `None` otherwise.

    """
    if not content:
        return False, None

    for i, line in enumerate(content):
        if """<div class="cal_termin_left">""" not in line:
            continue
        if "Kein Termin verfügbar" not in content[i + 3]:
            try:
                time = re.findall(r"[0-9]{1,2}:[0-9]{2}", content[i + 1])[0]
            except IndexError:
                time = None
            return True, time if time is not None else None
    return False, None


def get_date_from_url(url: str) -> datetime.datetime:
    """Get date from website URL.

    Parameters:
        url : str

    Returns:
        datetime.datetime
            Holds year, month, and day.

    """
    year = int(re.findall(r"&year=([0-9]{4})", url)[0])
    month = int(re.findall(r"&month=([0-9]{1,2})", url)[0])
    day = int(re.findall(r"&sel=([0-9]{1,2})", url)[0])
    return datetime.datetime(year, month, day)


def add_time_to_datetime(date: Union[datetime.date], time: str) -> datetime.datetime:
    """Add time to date.

    Parameters:
        date : datetime.datetime or datetime.date
        time : str

    Returns:
        datetime.datetime
            Holds year, month, day, hour and minute.

    """
    time_ = datetime.datetime.strptime(time, "%H:%M")
    delta = datetime.timedelta(hours=time_.hour, minutes=time_.minute)
    return date + delta


def format_datetime_to_str(date: datetime.datetime, strformat: str = "%d.%m.%Y %H:%M") -> str:
    """Format datetime to str.

    Parameters:
        date : datetime.datetime
        strformat : str
            Format of the string.

    Returns:
        str
            Formatted date.

    """
    return date.strftime(strformat)
