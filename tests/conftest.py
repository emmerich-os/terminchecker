import pathlib

import pytest

__my_loc__ = pathlib.Path(__file__).parent


@pytest.fixture()
def example_data_bytes():
    with open(__my_loc__ / "data/example.html", "rb") as f:
        return f.read()


@pytest.fixture()
def example_data_bytes_with_free_appointment():
    with open(__my_loc__ / "data/example_with_free_appointment.html", "rb") as f:
        return f.read()


@pytest.fixture()
def example_data_list():
    with open(__my_loc__ / "data/example.html") as f:
        content = f.read()
        return content.split("\n")


@pytest.fixture()
def example_data_list_with_free_appointment():
    with open(__my_loc__ / "data/example_with_free_appointment.html") as f:
        content = f.read()
        return content.split("\n")
